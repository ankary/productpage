import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./common/header/header.component";
import { NaviComponent } from "./common/navi/navi.component";
import { TrustedbyComponent } from "./common/trustedby/trustedby.component";
import { HowitworksComponent } from "./common/howitworks/howitworks.component";
import { CustompatchComponent } from "./common/custompatch/custompatch.component";
import { SpecialoptioncardComponent } from "./common/specialoptioncard/specialoptioncard.component";
import { EmbcoveragecardComponent } from "./common/embcoveragecard/embcoveragecard.component";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { WevemadeComponent } from "./common/wevemade/wevemade.component";
import { GalleryComponent } from "./common/gallery/gallery.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { Gallery2Component } from "./common/gallery2/gallery2.component";
import { ReviewsComponent } from "./common/reviews/reviews.component";
import { BlogComponent } from "./common/blog/blog.component";
import { BlogitComponent } from "./common/blogit/blogit.component";
import { AccordionModule } from "ngx-bootstrap/accordion";
import { FaqComponent } from "./common/faq/faq.component";
import { StartComponent } from "./common/start/start.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NaviComponent,
    TrustedbyComponent,
    HowitworksComponent,
    CustompatchComponent,
    SpecialoptioncardComponent,
    EmbcoveragecardComponent,
    WevemadeComponent,
    GalleryComponent,
    Gallery2Component,
    ReviewsComponent,
    BlogComponent,
    BlogitComponent,
    FaqComponent,
    StartComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot()
  ],

  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [GalleryComponent, Gallery2Component]
})
export class AppModule {}
