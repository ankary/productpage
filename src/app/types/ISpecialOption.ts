export interface ISpecialOption{

    name: string;
    description: string;
    image: string;
    mostPopular: boolean;

}