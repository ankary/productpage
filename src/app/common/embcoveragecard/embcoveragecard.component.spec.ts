import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmbcoveragecardComponent } from './embcoveragecard.component';

describe('EmbcoveragecardComponent', () => {
  let component: EmbcoveragecardComponent;
  let fixture: ComponentFixture<EmbcoveragecardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmbcoveragecardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmbcoveragecardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
