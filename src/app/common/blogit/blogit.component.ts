import { Component, OnInit, Input } from '@angular/core';
import { IBlogItem } from 'src/app/types/IBlogItem';

@Component({
  selector: 'app-blogit',
  templateUrl: './blogit.component.html',
  styleUrls: ['./blogit.component.scss']
})
export class BlogitComponent  {

  constructor() { }

  @Input() blogitem: IBlogItem;
 
}
