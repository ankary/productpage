import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery2',
  templateUrl: './gallery2.component.html',
  styleUrls: ['./gallery2.component.scss']
})
export class Gallery2Component {

  constructor() { }

  img : string = "/assets/images/gallery_7.svg";

  galleryImage(imgs) {
    this.img = imgs;
  };

}
