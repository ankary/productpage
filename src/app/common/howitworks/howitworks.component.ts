import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-howitworks',
  templateUrl: './howitworks.component.html',
  styleUrls: ['./howitworks.component.scss']
})
export class HowitworksComponent {

  leftImageURL: string = '/assets/images/accordion-image01.png'
  currentPosition: number = 1;

  constructor() { }

  selectOption(image: string, position: number) {
    this.leftImageURL = image;
    this.currentPosition = position;
  }
}
