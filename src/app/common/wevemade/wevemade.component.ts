import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { CarouselConfig } from "ngx-bootstrap/carousel";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { GalleryComponent } from "../gallery/gallery.component";
import { Gallery2Component } from "../gallery2/gallery2.component";

@Component({
  selector: "app-wevemade",
  templateUrl: "./wevemade.component.html",
  styleUrls: ["./wevemade.component.scss"],
  providers: [{ provide: CarouselConfig, useValue: { showIndicators: true } }]
})
export class WevemadeComponent implements OnInit {
  // carousel variables
  itemsPerSlide = 3;
  singleSlideOffset = false;
  noWrap = false;
  showIndicator = true;

  // show more/less
  status: string = "less";
  // slide = document.getElementById("slide2");
  // button = document.getElementById("moreless");

  @ViewChild("slide2", { static: false }) slide2: ElementRef;
  @ViewChild("moreless", { static: false }) moreless: ElementRef;

  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService) {}

  openGallery() {
    const initialState = {
      img: "/assets/images/gallery_1.svg"
    };
    this.bsModalRef = this.modalService.show(GalleryComponent, {
      initialState
    });
    this.bsModalRef.content.closeBtnName = "Close";
  }

  openGallery2() {
    const initialState = {
      img: "/assets/images/gallery_7.svg"
    };
    this.bsModalRef = this.modalService.show(Gallery2Component, {
      initialState
    });
    this.bsModalRef.content.closeBtnName = "Close";
  }

  ngOnInit() {}

  expandGallery() {
    console.log(this.slide2);
    if (this.status === "less") {
      this.slide2.nativeElement.style.display = "block";
      this.moreless.nativeElement.innerText = "See Less";
      this.status = "more";
    } else if (this.status === "more") {
      this.slide2.nativeElement.style.display = "none";
      this.moreless.nativeElement.innerText = "See More";
      this.status = "less";
    }
  }

  slides = [
    { image: "/assets/images/gallery_1.svg" },
    { image: "/assets/images/gallery_2.svg" },
    { image: "/assets/images/gallery_3.svg" },
    { image: "/assets/images/gallery_4.svg" },
    { image: "/assets/images/gallery_5.svg" },
    { image: "/assets/images/gallery_6.svg" },
    { image: "/assets/images/gallery_7.svg" },
    { image: "/assets/images/gallery_8.svg" },
    { image: "/assets/images/gallery_9.svg" },
    { image: "/assets/images/gallery_10.svg" },
    { image: "/assets/images/gallery_11.svg" },
    { image: "/assets/images/gallery_12.svg" }
  ];
}
