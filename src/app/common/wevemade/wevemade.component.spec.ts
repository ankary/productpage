import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WevemadeComponent } from './wevemade.component';

describe('WevemadeComponent', () => {
  let component: WevemadeComponent;
  let fixture: ComponentFixture<WevemadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WevemadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WevemadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
