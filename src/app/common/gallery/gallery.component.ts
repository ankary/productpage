import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {

  constructor() { }  

  img : string = "/assets/images/gallery_1.svg";

  galleryImage(imgs) {
    this.img = imgs;
  };
}
