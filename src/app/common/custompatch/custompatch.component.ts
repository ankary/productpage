import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-custompatch",
  templateUrl: "./custompatch.component.html",
  styleUrls: ["./custompatch.component.scss"]
})
export class CustompatchComponent {
  nrOfItemsPerSlide: number = 4;

  embcoveragecards = [
    {
      name: "50%",
      description:
        "Roughly 50% of your patch will have thread coverage. Best for text only designs.",
      image: "/assets/images/coverage-50.svg",
      mostPopular: false
    },
    {
      name: "75%",
      description: "Roughly 75% of your patch will have thread coverage.",
      image: "/assets/images/coverage-75.svg",
      mostPopular: false
    },
    {
      name: "100%",
      description:
        "Choose this option if you are looking for the closest color match to your design.",
      image: "/assets/images/coverage-100.svg",
      mostPopular: true
    }
  ];

  backings = [
    {
      name: "Iron-on Backing",
      description:
        "Adhesive backing that attaches to various fabrics when ironed.",
      image: "/assets/images/backing-0.svg",
      mostPopular: true
    },
    {
      name: "No Backing",
      description: "Roughly 50% of your patch will have thread coverage.",
      image: "/assets/images/backing-10.svg",
      mostPopular: false
    },
    {
      name: "Iron-On Backing Extending to Border",
      description:
        "Iron-on backing to the border, creating a flatter lay without the borders hanging from the garment.",
      image: "/assets/images/backing-11.svg",
      mostPopular: false
    },
    {
      name: "Velcro Hook Backing (Adhesive)",
      description: "Velcro Hook backing attached on the patch by adhesive.",
      image: "/assets/images/backing-12.svg",
      mostPopular: false
    },
    {
      name: "Velcro Loop Backing (Adhesive)",
      description:
        "Hook backing attached on the patch by adhesive, plus similarly-shaped loop surface backing.",
      image: "/assets/images/backing-1.svg",
      mostPopular: false
    },
    {
      name: "Adhesive Backing",
      description:
        "Peel-off cover for sticker-like application on regular surfaces.",
      image: "/assets/images/backing-2.svg",
      mostPopular: false
    },
    {
      name: "Thin Plastic Backing",
      description:
        "Thin plastic piece that adds structure and protects threads.",
      image: "/assets/images/backing-3.svg",
      mostPopular: false
    },
    {
      name: "Full Magnetic Backing",
      description:
        "Similarly-shaped bendable magnet for attaching to metal surfaces.",
      image: "/assets/images/backing-4.svg",
      mostPopular: false
    },
    {
      name: "Pin Backing",
      description: "Allows quick attachment/detachment to any fabric surface.",
      image: "/assets/images/backing-5.svg",
      mostPopular: false
    },
    {
      name: "Wearable Magnet",
      description: "Allows quick attachment/detachment to any fabric surface.",
      image: "/assets/images/backing-6.svg",
      mostPopular: false
    },
    {
      name: "Velcro Hook Backing(Edge Stitch)",
      description:
        "Velcro backing is sewn on the patch by running stitch on the edge.",
      image: "/assets/images/backing-7.svg",
      mostPopular: false
    },
    {
      name: "Velcro Hook Backing(Satin Stitch)",
      description: "Velcro Hook Backing is sewn on the patch by satin stitch.",
      image: "/assets/images/backing-8.svg",
      mostPopular: false
    },
    {
      name: "Velcro Hook Backing(Merrowed Border)",
      description: " ",
      image: "/assets/images/backing-9.svg",
      mostPopular: false
    }
  ];

  basematerials = [
    {
      name: "Polyester Blend Twill",
      description:
        "Adhesive backing that attaches to various fabrics when ironed.",
      image: "/assets/images/basematerials-0.svg",
      mostPopular: true
    },
    {
      name: "Felt",
      description: "Standard, matted material with great flexibility.",
      image: "/assets/images/basematerials-1.svg",
      mostPopular: false
    },
    {
      name: "Black Ballistic Nylon",
      description:
        "Premium nylon material with legendary durability (not bulletproof quality).",
      image: "/assets/images/basematerials-2.svg",
      mostPopular: false
    },
    {
      name: "Camouflage Material",
      description:
        "Camouflage background that's perfect for hunting or military patches.",
      image: "/assets/images/basematerials-3.svg",
      mostPopular: false
    },
    {
      name: "Reflective Material",
      description: "Premium Mylar background for added visibility at night.",
      image: "/assets/images/basematerials-4.svg",
      mostPopular: false
    }
  ];

  borderandedges = [
    {
      name: "Merrowed Border",
      description:
        "Edges sealed with an overlock stitch in color of your choice.",
      image: "/assets/images/borderandedges-0.svg",
      mostPopular: true
    },
    {
      name: "Embroidered Border",
      description: "Clean finish for semi-precise shapes.",
      image: "/assets/images/borderandedges-1.svg",
      mostPopular: false
    },
    {
      name: "Frayed Edges",
      description:
        "Edges secured with interior stitch, but left loose at perimeter.",
      image: "/assets/images/borderandedges-2.svg",
      mostPopular: false
    },
    {
      name: "No Border",
      description: "Stitches are secured with simple interior loop stitch.",
      image: "/assets/images/borderandedges-3.svg",
      mostPopular: false
    }
  ];

  premiumthreadoptions = [
    {
      name: "Metallic Thread",
      description: "Premium metallic threads add shine and luxury.",
      image: "/assets/images/premiumthreadoptions-0.svg",
      mostPopular: false
    },
    {
      name: "Neon Thread",
      description: "Premium neon threads add vibrancy and interest.",
      image: "/assets/images/premiumthreadoptions-1.svg",
      mostPopular: false
    },
    {
      name: "Extra Thread Colors (10 - 12)",
      description: "Select if your design needs 10+ colors.",
      image: "/assets/images/premiumthreadoptions-2.svg",
      mostPopular: false
    },
    {
      name: "Glow in the Dark (10% Embroidery)",
      description:
        "Roughly 10% of your patch will be covered with luminescent threads.",
      image: "/assets/images/premiumthreadoptions-4.svg",
      mostPopular: false
    },
    {
      name: "Puff Embroidery (3D)",
      description: "Stitches over raised backing creates a puffy, 3D effect.",
      image: "/assets/images/premiumthreadoptions-3.svg",
      mostPopular: false
    },
    {
      name: "Glow in the Dark (25% Embroidery)",
      description:
        "Roughly 25% of your patch will be covered with luminescent threads.",
      image: "/assets/images/premiumthreadoptions-5.svg",
      mostPopular: false
    },
    {
      name: "Glow in the Dark (50% Embroidery)",
      description:
        "Roughly 50% of your patch will be covered with luminescent threads.",
      image: "/assets/images/premiumthreadoptions-6.svg",
      mostPopular: false
    },
    {
      name: "12 or More Thread Colors",
      description: " ",
      image: "/assets/images/premiumthreadoptions-7.svg",
      mostPopular: false
    }
  ];

  upgrades = [
    {
      name: "Button Loop",
      description: "Thick loop is added to top of patch.",
      image: "/assets/images/upgrades-0.svg",
      mostPopular: true
    },
    {
      name: "Swarovski Crystals",
      description: "Swarovski-brand crystals to be embedded per your design.",
      image: "/assets/images/upgrades-1.svg",
      mostPopular: false
    }
  ];

  lasercuts = [
    {
      name: "Hot Cut Edge",
      description: "Less Fabric and better heat-sealed edge.",
      image: "/assets/images/borderandedges-0.svg",
      mostPopular: true
    },
    {
      name: "Laser Cut or Hand Cut",
      description:
        "Both Laser Cut and Hand Cut are  With <=1mm fabric edge along the border, FTY will make it according to the design.",
      image: "/assets/images/placeholder.svg",
      mostPopular: false
    }
  ];

  constructor() {
    this.checkScreen();
  }

  checkScreen() {
    if (window.innerWidth < 400) {
      this.nrOfItemsPerSlide = 1;
    } else {
      this.nrOfItemsPerSlide = 4;
    }
  }
}
