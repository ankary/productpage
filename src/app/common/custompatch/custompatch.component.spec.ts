import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustompatchComponent } from './custompatch.component';

describe('CustompatchComponent', () => {
  let component: CustompatchComponent;
  let fixture: ComponentFixture<CustompatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustompatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustompatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
