import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialoptioncardComponent } from './specialoptioncard.component';

describe('SpecialoptioncardComponent', () => {
  let component: SpecialoptioncardComponent;
  let fixture: ComponentFixture<SpecialoptioncardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialoptioncardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialoptioncardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
