import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent {

  blogItems = [{
    image: "/assets/images/blog_1.svg",
    description: "Patches 101: The 4 Most Important Options"
  },
  {
    image: "/assets/images/blog_2.svg",
    description: "Patches 101: The 4 Most Important Options"
  },
  {
    image: "/assets/images/blog_3.svg",
    description: "Patches 101: The 4 Most Important Options"
  }
];

  constructor() { }  
}
